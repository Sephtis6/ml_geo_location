﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.XR.MagicLeap;
using System;
using System.Collections;
using System.Collections.Generic;

namespace MagicLeap
{
    [RequireComponent(typeof(PrivilegeRequester))]

    public class GeoLocator : MonoBehaviour
    {
        private PrivilegeRequester _privilegeRequester;
        private Text _locationText = null;

        public float longitude;
        public float latitude;
        public Text longitudeText;
        public Text latitudeText;
        public Text timeText;

        public bool isDay = true;

        public GameObject dayScene;
        public GameObject nightScene;

        public List<AudioClip> christmasSongsToPlay;
        public List<AudioClip> halloweenSongsToPlay;
        public AudioSource audioSource;
        public int positionInSongs = 0;

        public float time;

        //Start the privilege requester system in Awake()
        private void Awake()
        {
            _privilegeRequester = GetComponent<PrivilegeRequester>();
            if (_privilegeRequester)
            {
                // Register event listeners.
                _privilegeRequester.OnPrivilegesDone += HandlePrivilegesDone;
            }
            time = ((float)DateTime.Now.Hour + ((float)DateTime.Now.Minute * 0.01f));
            timeText.text = "Time: " + time.ToString();
            if (time > 7 & time < 19)
            {
                isDay = true;
            }
            else if (time <= 7 || time >= 19)
            {
                isDay = false;
            }

            if (isDay == true)
            {
                dayScene.SetActive(true);
                nightScene.SetActive(false);
                audioSource.clip = christmasSongsToPlay[positionInSongs];
                audioSource.Play();
                positionInSongs = positionInSongs + 1;
            }
            else
            {
                nightScene.SetActive(true);
                dayScene.SetActive(false);
                audioSource.clip = halloweenSongsToPlay[positionInSongs];
                audioSource.Play();
                positionInSongs = positionInSongs + 1;
            }
        }

        //Stop the privilege requester system in OnDestroy()
        void OnDestroy()
        {
            if (_privilegeRequester != null)
            {
                // Unregister event listeners.
                _privilegeRequester.OnPrivilegesDone -= HandlePrivilegesDone;
            }
        }

        private void HandlePrivilegesDone(MLResult result)
        {
            if (!result.IsOk)
            {
                if (result.Code == MLResultCode.PrivilegeDenied)
                {
                    Instantiate(Resources.Load("PrivilegeDeniedError"));
                }

                Debug.LogErrorFormat("Error: LocationExample failed to get requested privileges, disabling script. Reason: {0}", result);
                enabled = false;
                return;
            }

            StartupAPI();
        }

        /// Starts the MLLocation API and polls data if needed.
        private void StartupAPI()
        {
            MLLocation.Start();
        }

        public void Update()
        {
            if (isDay == true)
            {
                if (!audioSource.isPlaying)
                {
                    audioSource.clip = christmasSongsToPlay[positionInSongs];
                    audioSource.Play();
                    if (positionInSongs > christmasSongsToPlay.Count)
                    {
                        positionInSongs = 0;
                    }
                    else
                    {
                        positionInSongs = positionInSongs + 1;
                    }
                }
            }
            else if (isDay == false)
            {
                if (!audioSource.isPlaying)
                {
                    audioSource.clip = halloweenSongsToPlay[positionInSongs];
                    audioSource.Play();
                    if (positionInSongs > halloweenSongsToPlay.Count)
                    {
                        positionInSongs = 0;
                    }
                    else
                    {
                        positionInSongs = positionInSongs + 1;
                    }
                }
            }
            GetLocation();
            longitudeText.text = "Longitude: " + longitude.ToString();
            latitudeText.text = "Latitude: " + latitude.ToString();
        }

        /// Polls current location data.
        private void GetLocation()
        {
            MLLocationData newData = new MLLocationData();
            MLResult result = MLLocation.GetLastCoarseLocation(out newData);
            if (result.IsOk)
            {
                _locationText.text = String.Format(
                    "Latitude:\t<i>{0}</i>\n" +
                    "Longitude:\t<i>{1}</i>\n" +
                    "Postal Code:\t<i>{2}</i>",
                    newData.Latitude,
                    newData.Longitude,
                    newData.HasPostalCode ? newData.PostalCode : "(unknown)"
                );
                longitude = newData.Longitude;
                latitude = newData.Latitude;
            }   
        }
    }
}